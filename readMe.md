# EPE

## Emotional Typography

Content source : *Emotional Typography: The Effectiveness of Current Typographic Design Categories in Eliciting Emotion*, Amic G. Ho; January 2017, The International Journal of Visual Design 11(2):37-44

## Authors
Andrea Kevorkian - Esad•V, Çağla Gül Dereli - IUE VCD

# General concept

The text by author *Amic G. Ho* tries to identifies how typography can express emotions through their very drawing. Several categories associating fonts to emotions are suggested that more or less make sense. To make a soft critic of this ideas, we decided to ask ChatGPT ta make a selection of fonts for us according to the emotions listed in this article.
The resulting list of fonts is used to produce page that shows the original text with in a font specimen like layout. When the user scrolls up or down, some letter's font are randomly changed to make use of these fonts "emotional expression" capacity.
The printed version uses the text in its current shape, each print out being particular.