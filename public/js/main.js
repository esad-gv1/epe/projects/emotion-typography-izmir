const fontFamilies = {
    'texte': ['Synt', 'BHV', 'Clearface', 'NewCentury', 'ABCMaxiRound'],
    'texte1': ['BasteB', 'CLMGregam', 'Rauschen', 'ABCConnect'],
    'texte2': ['Prelettre', 'LED-Dot', 'Metaballs', 'MoonbaseAlpha', 'OPSKappla', 'OPSPastPerfect', 'OPSRestructionalText', 'BoogyBrut'],
    'texte3': ['GillSansStd', 'ShelleyLTStd', 'Snell Roundhand', 'ABCGintoNordCondensed'],
    'texte4': ['ABCGintoRoundedNordCondensed', 'ABCMonumentGrotesk', 'BauhausStd', 'DIN BlackAlternate', 'Fugue', 'LL-Replica', 'NHaasGroteskTXPro', 'SupremeLL'],
    'texte5': ['Cirrus', 'COMICSANSBOLD', 'OPSUsedFuture', 'DinaChaumont', 'WF-000099']
};

let textContainers;

export function spanitize() {
    textContainers = document.querySelectorAll('.texte, .texte1, .texte2, .texte3, .texte4, .texte5');
    textContainers.forEach((textContainer) => {
        const originalText = textContainer.textContent;
        textContainer.innerHTML = '';
        textContainer.appendChild(wrapTextInSpans(originalText));
    });
    console.log("textContainers", textContainers);
}
var changedCharacters = new Set();

// texte en spans
function wrapTextInSpans(text) {
    var words = text.trim().split(/\s+/);
    var fragment = document.createDocumentFragment();
    words.forEach(function (word) {
        var wordSpan = document.createElement('span');
        Array.from(word).forEach(function (char) {
            var charSpan = document.createElement('span');
            charSpan.textContent = char;
            wordSpan.appendChild(charSpan);
        });
        wordSpan.appendChild(document.createTextNode(' '));
        fragment.appendChild(wordSpan);
    });
    console.log(fragment)
    return fragment;
}

/* function resetChangedCharacters() {
    changedCharacters.clear();
} */

var lastScrollTime = 0;
var animationFrameId = null;
var isScrolling = false;

function changeTypography() {
    var maxCharactersToChange = 1; // nombre max caractères (défilement)

    console.log(textContainers)
    textContainers.forEach(function (textContainer) {
        var spans = textContainer.querySelectorAll('span span');
        var totalCharacters = spans.length;
        var fontFamilyList = fontFamilies[textContainer.classList[0]];
        //console.log("fontFamilyList", fontFamilyList);
        var charactersToChange = Math.ceil(Math.random() * maxCharactersToChange); // Nombre aléatoire de caractères à changer
        var changedCount = 0;

        while (changedCount < charactersToChange) {
            console.log("changedCount < charactersToChange", changedCount < charactersToChange);
            var randomIndex = Math.floor(Math.random() * totalCharacters);
            var span = spans[randomIndex];
            if (!changedCharacters.has(span)) {
                var fontFamily = fontFamilyList[Math.floor(Math.random() * fontFamilyList.length)];
                span.style.fontFamily = fontFamily;
                changedCharacters.add(span);
                changedCount++;
            }
        }
    });

    // Vérifie si le bas de la page est atteint
    if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight) {
        isScrolling = false;
    }
}

let scrollListenerHandler = function () {
    //isScrolling = true;
    changeTypography();
}

window.addEventListener('scroll', scrollListenerHandler);

export function removeScrollListener() {
    console.log("removeScrollListener");
    window.removeEventListener('scroll', scrollListenerHandler);
}

